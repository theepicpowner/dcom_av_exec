# DCOM_AV_EXEC (+ AV_Bypass_Framework_V3)

**Summary:**

DCOM_AV_EXEC allows for "diskless" lateral movement to a target on the same network via DCOM. The AV_Bypass_Framework_V3 creates a .NET shellcode 
runner (output as DLL) which can be used with the DCOM_AV_EXEC tool to bypass antivirus solutions like Microsoft Defender as all shellcode is AES encrypted and executed in memory. 

In order to use DCOM_AV_EXEC you need to have local administrator permissions on the target ( e.g this can be via a stolen/crafted token on your current Cobalt Strike Beacon or for example by having an existing session as a local administrator on your beacon, which also has equivalent permissions on the target - domain user with local admin privs on both machines).
 
Depending on the detection mechanisms present on target network, DCOM lateral movement could be beneficial. Tools such as 
like psexec or or wmiexec are noisy and often monitored. DCOM lateral movement may still be able to go undetected under the right 
cicumstances.

More info on DCOM: https://www.cybereason.com/blog/dcom-lateral-movement-techniques

**Usage:**

_Requires:_

- Local Admin access on target from existing beacon session to leverage DCOM (elevated token or current session as same local admin) 
- AV_Framework_V3 on native windows (https://gitlab.com/theepicpowner/av_bypass_framework_v3) 
- DCOM_AV_EXEC within the teamserver host
- CSSG_LOAD.cna (https://github.com/RCStep/CSSG)
- Check trust between foothold machine and DC/target (Test-ComputerSecureChannel)

_Steps:_

- Load ccsg_load.cna into Cobalt Strike (https://github.com/RCStep/CSSG)
- Generate stageless shellcode for either HTTP, HTTPS, SMB or TCP listeners with the CCSG CNA (b64 C# shellcode output)
- Store the outputed content in a file and run it through AV_Framework_V3 (AV_Bypass_Framework_V3.exe <path to file>)
- Host the AutoGen DLL in Cobalt Strike (Host File)
- Point Cradle.ps1 to the DLL hosted in Cobalt Strike. Also host the Cradle.ps1 in Cobalt Strike (Host File)
- Execute DCOM_AV_EXEC with the target and hosted cradle.ps1 URL as arguments
- Connect or link to target

**Examples Usage - TCP Listener Payload:**

Create Shellcode with CCSG - Encoding: b64 | Format: 0x90, 0x8e, .. 
![CCSG](Examples/shellcode_tcp_listener.png)
![CCSG_1](Examples/shellcode_tcp_listener_1.png)

Save CCSG output to file and run it through AV_Bypass_Framework_V3 (needs compiling first) on Windows:

`AV_Bypass_Framework_V3.exe <PATH_TO_B64_SHELLCODE_FROM_CSSG_CNA>`

Copy outputed cradles.ps1 and AutoGen.dll to Cobalt Strike teamserver host. Host the AutoGen.dll in the teamserver (Host File).
![host_dll](Examples/host_autogen_dll.png)

Following this edit the cradle.ps1 file to point to the teamserver hosted AutoGen.dll file URL:
![cradle_edit](Examples/cradle_edit.png)

Host the cradle.ps1 file on the Cobalt Strike teamserver too:
![host_cradle](Examples/host_cradle_ps1.png)

Cobalt Strike sites should now show both hosted files:
![hosted_files](Examples/hosted_files_cobalt.png)

Execution. Compile and upload the DCOM_AV_EXEC to the Cobalt Strike teamserver. Then:
![ex](Examples/execution_1.png)
![ex1](Examples/execution_2.png)
![ex2](Examples/execution_beacon.png)
![ex3](Examples/beacon_target.png)

**TODO:**
- Test DNS listener payload 
- Check for footprint on SIEM e.g Splunk 
- Test against commerical EDR/AV solutions
- Obfuscate created DLL in AV_Bypass_Framework_V3 
